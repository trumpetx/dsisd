/*
@author David Greene - 2016
 */
var PERMALINK_VERSION = 'f';
var zones = {};
var elementaries;
var years = ['2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025'];
var binaryHash = {
	'sse': 0,
	'wse': 1,
	'dse': 2,
	'roe': 3,
	'el5': 4,
	'el6': 5
};
var el5_option1;
var el5_option2;
var binaryHashDecode = _.invert(binaryHash);

var binaryHashMiddle = {
	'srm': 0,
	'dsm': 1
};
var binaryHashMiddleDecode = _.invert(binaryHashMiddle);

var service;
var directionsDisplay;
var params = {};
location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
	params[k] = v;
});

$(function () {
	if (location.pathname.indexOf('beta') > 0) {
		$beta = $('<div/>', {
				'class': 'beta-header',
				html: 'BETA VERSION: some features may not be fully implemented'
			});
		$('html').prepend($beta);
	}

	if (params.permalink) {
		$('#custom_option').prop('checked', true);
	} else {
		$('#option1').prop('checked', true);
	}

	var toggleCalculateRoute = function () {
		$('#route_info').toggle($('#calculate_route').is(':checked'));
	};
	$('#calculate_route').on('click', toggleCalculateRoute);
	toggleCalculateRoute();

	loadZoneData();
	var map = initGoogleMap();

	populateSchools(map);
	drawZones(map);
	$('input[name=zoning_option]').on('change', function () {
		if ($(this).val() !== 'custom_option') {
			history.replaceState(undefined, undefined, location.pathname.substring(0, location.pathname.lastIndexOf('/') + 1));
		}
		initPlanningMap();
	});
	initPlanningMap(true);

	$('#permalink').on('click contextmenu', function (evt) {
		calculatePermaLink($(this));
	});

	addToggleToLegend();
	handleFutureElementaries();
});

function readTextFile(file) {
	var rawFile = new XMLHttpRequest();
	rawFile.open('GET', file, false);
	var data = '';
	rawFile.onreadystatechange = function () {
		if (rawFile.readyState === 4) {
			if (rawFile.status === 200 || rawFile.status == 0) {
				data = rawFile.responseText;
			}
		}
	}
	rawFile.send(null);
	return data;
}

function rgb(hex, fakeOpacity) {
	var bigint = parseInt(hex, 16);
	var r = (bigint >> 16) & 255;
	var g = (bigint >> 8) & 255;
	var b = bigint & 255;
	var overColor = 255;
	r = overColor + (r - overColor) * fakeOpacity;
	g = overColor + (g - overColor) * fakeOpacity;
	b = overColor + (b - overColor) * fakeOpacity;
	return 'rgb(' + Math.round(r) + ',' + Math.round(g) + ',' + Math.round(b) + ')';
}

function change(zone, newElementaryKey) {
	if (newElementaryKey) {
		var oldElementary = elementaries[zone.elementary];
		zone.elementary = newElementaryKey;
		_.forEach(years, function (year) {
			oldElementary[year] -= _.parseInt(zone[year]);
		});
		$('#custom_option').prop('checked', true);
	}
	var newElementary = elementaries[zone.elementary];
	if (newElementary) {
		_.forEach(years, function (year) {
			newElementary[year] += _.parseInt(zone[year]);
		});

		zone.zonePoly.setOptions({
			fillColor: newElementary.color
		});
		if (newElementaryKey) {
			var driveTime = new Date();
			driveTime.setDate(driveTime.getDate() + 1);
			driveTime.setHours(7);
			driveTime.setMinutes(30);
			driveTime.setMilliseconds(0);
			if ($('#calculate_route').is(':checked')) {
				service.route({
					origin: zone.centerMarker.getPosition(),
					destination: newElementary.marker.getPosition(),
					travelMode: google.maps.DirectionsTravelMode.DRIVING,
					provideRouteAlternatives: false,
					transitOptions: {
						arrivalTime: driveTime
					}
				}, function (result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(result);
						$('#route_duration').val(result.routes[0].legs[0].duration.text);
						$('#route_distance').val(result.routes[0].legs[0].distance.text);
					}
				});
			}
		}
	}
}

function initGoogleMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 12,
			mapTypeId: google.maps.MapTypeId.HYBRID,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.SATELLITE]
			},
			disableDoubleClickZoom: true,
			scrollwheel: false,
			draggableCursor: 'crosshair',
			center: {
				lat: 30.212793,
				lng: -98.072734
			},
			mapTypeId: 'terrain'
		});
	directionsDisplay = new google.maps.DirectionsRenderer({
			map: map,
			preserveViewport: true
		});
	service = new google.maps.DirectionsService();
	return map;
}

function toggleElementary(key, show) {
	var elementary = elementaries[key];
	elementary.show = show;
	$('.v-' + key).toggle(show);
	if (elementary.marker) {
		elementary.marker.setVisible(show);
	}
}

function handleFutureElementaries() {
	var hideYears = function () {
		var el5Checked = $('#elementary_5').is(':checked');
		var el6Checked = $('#elementary_6').is(':checked');
		var elem5StartYear = _.parseInt($('#elementary-5-start').val());
		var minYear = 2017;
		var maxYear = elem5StartYear;
		if (el6Checked) {
			$('#elementary_5').prop('checked', true);
			$('#elementary_5').prop('disabled', true);
			minYear = _.parseInt($('#elementary-6-start').val());
			maxYear = 2030;
		} else if (el5Checked) {
			$('#elementary_5').prop('disabled', false);
			minYear = elem5StartYear;
			maxYear = _.parseInt($('#elementary-6-start').val());
		}
		_.forEach(years, function (y) {
			var year = _.parseInt(y);
			if (year >= minYear && year <= maxYear) {
				$('.y-' + y).show();
			} else {
				$('.y-' + y).hide();
			}
		});
		$('#custom_option').prop('checked', $('#custom_option').is(':checked') || el5Checked || el6Checked);
		toggleElementary('el5', el5Checked || el6Checked);
		toggleElementary('el6', el6Checked);
		$('.future5-option').toggle(el5Checked || el6Checked);
		$('.future6-option').toggle(el6Checked);
		$('.existing-option').toggle(!(el5Checked || el6Checked));
		_.forEach(zones, function (zone) {
			if ((zone.elementary === 'el5' && !el5Checked) || (zone.elementary === 'el6' && !el6Checked)) {
				change(zone, zone.option1);
			}
		});
	};

	var onElem5Change = function () {
		toggleElementary('el5', false);
		elementaries.el5 = ($('#elementary-5-location option:selected').val() === 'loc1')
		 ? el5_option1
		 : el5_option2
		toggleElementary('el5', $('#elementary_5').is(':checked') || $('#elementary_6').is(':checked'));
	};
	
	$('.adjust-years').on('click', hideYears);
	hideYears();
	$('#elementary-5-location').on('change', onElem5Change);
	onElem5Change();
}

function addToggleToLegend() {
	var $i = $('<i/>', {
			'class': 'fa fa-minus-square-o',
			'aria-hidden': 'true'
		});

	$('legend.togvis').append($i);

	$('legend.togvis').click(function () {
		$(this).parents('fieldset').find('*:not("legend"):not("i")').toggle();
		$(this).find('i').toggleClass('fa-minus-square-o');
		$(this).find('i').toggleClass('fa-plus-square-o');
		return false;
	});
}

function calculatePermaLink(a) {
	var linkString = [];
	_.forEach(zones, function (zone) {
		var middle = zone.middle;
		if (!middle) {
			switch (zone.elementary) {
			case 'sse':
			case 'roe':
				middle = 'srm';
				break;
			case 'dse':
			case 'wse':
				middle = 'dsm';
				break;
			}
		}
		var midBin = binaryHashMiddle[middle];
		var bin = binaryHash[zone.elementary];
		linkString.push((midBin << 3 | bin).toString(16));
	});
	a.attr('href', '?permalink=' + linkString.reverse().join('') + ($('#elementary-5-location option:selected').val() === 'loc1' ? 0 : 1) + PERMALINK_VERSION);
}

function school(label, color, capacity, lat, lng, map, show, iconUrl) {
	if (!iconUrl) {
		iconUrl = 'http://maps.google.com/mapfiles/kml/shapes/schools.png';
	}
	return {
		label: label,
		marker: (lat && lng) ? new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			title: label,
			map: map,
			visible: show,
			icon: {
				url: iconUrl,
				size: new google.maps.Size(64, 64),
				scaledSize: new google.maps.Size(32, 32),
				anchor: new google.maps.Point(8, 24)
			}
		}) : undefined,
		color: color,
		capacity: capacity,
		show: show
	}
}

function loadZoneData() {
	var gis = populateGis();
	var parsed = Papa.parse(readTextFile('http://trumpetx.com/' + location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1) + '/dsisd-02-2017.csv'), {
			encoding: 'UTF-8',
			header: true,
			dynamicTyping: false,
			delimiter: ',',
			fastmode: false,
			download: false,
			skipEmptyLines: true,
			newline: '\r\n'
		});

	_.forEach(parsed.data, function (zone) {
		var g = gis[zone.planning_unit];
		if (g) {
			zone.gis = g;
		}
		zones[zone.planning_unit] = zone;
	});
}

function populateSchools(map) {
	el5_option1 = school('Elementary #5', '#0260f7', 850, 30.148121, -98.095218, map, false);
	el5_option2 = school('Elementary #5', '#0260f7', 850, 30.153353, -98.008021, map, false);
	elementaries = {
		wse: school('Walnut Springs Elementary', '#008000', 850, 30.1947757, -98.0948766, map, true),
		dse: school('Dripping Springs Elementary', '#FF0000', 900, 30.2158518, -98.0825343, map, true),
		roe: school('Rooster Springs Elementary', '#800080', 850, 30.1920847, -97.9828326, map, true),
		sse: school('Sycamore Springs Elementary', '#FFFF00', 850, 30.180898, -98.001506, map, true),
		el5: el5_option1, // onChange() executed later
		el6: school('Elementary #6', '#444444', 850, 30.209563, -98.017966, map, $('#elementary_6').is(':checked'), 'http://maps.google.com/mapfiles/ms/micons/question.png')
	};
	toggleElementary('el5', $('#elementary_5').is(':checked'));

	_.forEach(elementaries, function (elem, key) {
		var $elementaryFieldset = $('<fieldset/>', {
				'class': 'v-' + key
			});
		$elementaryFieldset.append($('<legend/>', {
				html: elem.label,
				style: 'background-color: ' + rgb(elem.color.substring(1), .35) + ';',
				id: 'legend_' + key,
				'class': 'togvis'
			}));
		_.forEach(years, function (year) {
			elem[year] = 0;
			$div = $('<div/>', {
					'class': 'year y-' + year
				});
			$div.append($('<label/>').attr('for', key + '_' + year).text(year));
			$div.append($('<input/>', {
					'type': 'text',
					id: key + '_' + year,
					name: key + '_' + year,
					value: 0,
					'class': 'number-input',
					disabled: true
				}));
			$elementaryFieldset.append($div);
		});
		$('#middle-right').append($elementaryFieldset);
	});
}

function setZoneCounts() {
	_.forEach(elementaries, function (cts, elem) {
		_.forEach(years, function (year) {
			$enrollment = $('#' + elem + '_' + year);
			$enrollment.val(cts[year]).css('background-color', function () {
				if (cts[year] < cts.capacity) {
					return 'white';
				}
				if (cts[year] < cts.capacity * 1.2) {
					return 'yellow';
				}
				return 'red';
			});
		});
	});
}

/*
Returns a version for the permalink - if no version is set the value will be a hex character and the version is assumed to be 'h'

Versions:
h = initial version
g = includes data for middle school assignemnts and possible elementary 5 & 6
f = includes zones 37a/b 34a/b
 */
function parsePermaLink(permalinkString) {
	var permalink = permalinkString.split('');
	var testChar = permalink[permalink.length - 1].toLowerCase();
	var version = ((parseInt(testChar, 16) == testChar) ? 'h' : permalink.pop()).toLowerCase();

	var splitZones = [34, 37];
	var splitComplete = [];

	switch (version) {
	case 'f':
		var next = parseInt(permalink.pop(), 16);
		if (next === 0) {
			$('#elementary-5-location').val('loc1');
		} else {
			$('#elementary-5-location').val('loc2');
		}
	case 'g':
		_.forEach(zones, function (zone) {
			var check = _.toInteger(zone.planning_unit.replace(/[^0-9]+/g, ''));
			if (_.includes(splitComplete, check)) {
				return;
			}
			var next = parseInt(permalink.pop(), 16);
			zone.elementary = binaryHashDecode[next & 7];
			if (zone.elementary === 'el5') {
				$('#elementary_5').prop('checked', true);
			} else if (zone.elementary === 'el6') {
				$('#elementary_6').prop('checked', true);
			}
			zone.middle = binaryHashMiddleDecode[next >> 3];
			change(zone);
			if (version === 'g' && _.includes(splitZones, check)) {
				var splitZone = 'pu' + check + (_.endsWith(zone.planning_unit, 'a') ? 'b' : 'a');
				zones[splitZone].elementary = zone.elementary;
				zones[splitZone].middle = zone.middle;
				change(zones[splitZone]);
				_.pull(splitZones, check);
				splitComplete.push(check);
			}
		});
		break;
	case 'h':
	default:
		var next;
		var i = 0;
		_.forEach(zones, function (zone) {
			var check = zone.planning_unit.replace(/[^0-9]+/g, '');
			if (splitComplete.includes(check)) {
				return;
			}
			if (i % 2 === 0) {
				next = parseInt(permalink.pop(), 16);
				zone.elementary = binaryHashDecode[next >> 2];
			} else {
				zone.elementary = binaryHashDecode[next & 3];
			}
			change(zone);
			i++;
			if (splitZones.includes(check)) {
				var splitZone = 'pu' + check + (_.endsWith(zone.planning_unit, 'a') ? 'b' : 'a');
				zones[splitZone].elementary = zone.elementary;
				zones[splitZone].middle = zone.middle;
				change(zones[splitZone]);
				_.pull(splitZones, check);
				splitComplete.push(check);
			}
		});
	}
}

function initPlanningMap(populatePermaLink) {
	var selectedPlanningMap = $('input[name=zoning_option]:checked').val();
	if (selectedPlanningMap === 'custom_option' && !populatePermaLink) {
		return;
	}
	_.forEach(elementaries, function (elem) {
		_.forEach(years, function (year) {
			elem[year] = 0;
		});
	});
	if (!populatePermaLink || !params.permalink) {
		_.forEach(zones, function (zone) {
			zone.elementary = zone[selectedPlanningMap];
			change(zone);
		});
	} else {
		parsePermaLink(params.permalink);
	}
	setZoneCounts();
}

function drawZones(map) {
	_.forEach(zones, function (zone) {
		if (zone.gis) {
			var zonePoly = new google.maps.Polygon({
					paths: zone.gis,
					strokeColor: '#666',
					strokeOpacity: 0.8,
					strokeWeight: 2,
					fillColor: '#CCC',
					fillOpacity: 0.35
				});
			zone.bounds = new google.maps.LatLngBounds();
			_.forEach(zone.gis, function (latLng) {
				zone.bounds.extend(latLng);
			});
			zone.centerMarker = new google.maps.Marker({
					position: zone.bounds.getCenter(),
					title: zone.planning_unit,
					map: map,
					visible: false,
					draggable: false
				});
			var mapLabel = new MapLabel({
					text: zone.planning_unit,
					position: zone.centerMarker.getPosition(),
					map: map,
					fontSize: 10,
					align: 'center'
				});

			zone.centerMarker.bindTo('map', mapLabel);
			zone.centerMarker.bindTo('position', mapLabel);

			zonePoly.setMap(map);
			google.maps.event.addListener(zonePoly, 'click', function (evt) {
				var elems = _.keys(elementaries);
				var next = _.indexOf(elems, zone.elementary) + 1;
				var newElementary;
				while (!newElementary) {
					if (next === elems.length) {
						next = 0;
					}
					if (elementaries[elems[next]].show) {
						newElementary = elems[next];
					}
					next++;
				}
				change(zone, newElementary);
				setZoneCounts();
			});
			zone.zonePoly = zonePoly;
		}
	});
}
